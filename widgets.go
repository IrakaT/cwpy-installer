package main

import (
	"bytes"
	"image"

	"github.com/lxn/walk"
	decl "github.com/lxn/walk/declarative"
)

func headerWidget() decl.Widget {
	size := decl.Size{Width: 400, Height: 70}
	img, _, _ := image.Decode(bytes.NewBuffer(MustAsset("logo.png")))
	wImg, _ := walk.NewBitmapFromImage(img)
	return decl.Composite{
		Background: decl.BitmapBrush{
			Image: wImg,
		},
		MaxSize: size,
		MinSize: size,
		Font: decl.Font{
			Family:    font,
			PointSize: 8,
		},
		Layout: decl.Grid{
			Columns: 2,
			Margins: decl.Margins{
				Right: 10,
			},
			MarginsZero: true,
			SpacingZero: true,
		},
		Children: []decl.Widget{
			decl.VSpacer{ColumnSpan: 2},
			decl.HSpacer{},
			decl.LinkLabel{
				Text: "<a></a><a>Languages and Libraries</a>",
				// 空リンクは開始時のフォーカスを防ぐため
				OnLinkActivated: showLicenses,
			},
		},
	}
}

func showLicenses(link *walk.LinkLabelLink) {
	if link.Index() == 1 {
		size := decl.Size{Height: 400}
		decl.Dialog{
			Title:     appTitle,
			Icon:      rsrcIco,
			MaxSize:   size,
			MinSize:   size,
			FixedSize: true,
			Font: decl.Font{
				Family:    font,
				PointSize: 9,
			},
			Layout: decl.VBox{
				MarginsZero: true,
			},
			Children: []decl.Widget{
				decl.ScrollView{
					HorizontalFixed: true,
					Layout: decl.VBox{
						MarginsZero: true,
					},
					Children: []decl.Widget{
						decl.LinkLabel{
							Text: string(MustAsset("license.txt")),
						},
					},
				},
			},
		}.Run(mainWin)
	}
}

func optionsWidget() decl.Widget {
	return decl.Composite{
		Layout: decl.Grid{
			Columns: 2,
			Margins: decl.Margins{
				Top:    10,
				Left:   20,
				Right:  20,
				Bottom: 10,
			},
			SpacingZero: true,
		},
		Children: []decl.Widget{
			decl.Label{Text: "エンジン", MinSize: decl.Size{Width: 80}},
			engineSelectWidget(),

			decl.VSpacer{Size: 5, ColumnSpan: 2},

			decl.Label{Text: "エディタ"},
			editorSelectWidget(),
			decl.Label{},
			editorFolderWidget(),

			decl.VSpacer{Size: 5, ColumnSpan: 2},

			decl.Label{Text: "インストール先"},
			pathWidget(),
		},
	}
}

func engineSelectWidget() decl.Widget {
	return selectWidget("EngineID", binding.engines)
}
func editorSelectWidget() decl.Widget {
	return selectWidget("EditorID", binding.editors)
}
func selectWidget(key string, model []*archiveData) decl.Widget {
	return decl.ComboBox{
		Value:         decl.Bind(key),
		BindingMember: "ID",
		DisplayMember: "Name",
		Model:         model,
	}
}

func editorFolderWidget() decl.Widget {
	// Text を Bind したいが手法が見つからない
	return decl.CheckBox{
		Checked: decl.Bind("SubFolder"),
		Text:    "cwxeditorフォルダに入れる",
	}
}

func pathWidget() decl.Widget {
	return decl.LineEdit{
		Text:     exeDir,
		ReadOnly: true,
	}
}

func manualWidget() decl.Widget {
	size := decl.Size{Height: 160}
	return decl.TextEdit{
		Text: string(MustAsset("manual.txt")),
		Font: decl.Font{
			Family:    font,
			PointSize: 9,
		},
		MaxSize:  size,
		MinSize:  size,
		ReadOnly: true,
		VScroll:  true,
	}
}

func buttonsWidget() decl.Widget {
	return decl.Composite{
		Layout: decl.HBox{
			Margins: decl.Margins{
				Top:    10,
				Left:   20,
				Right:  20,
				Bottom: 20,
			},
			Spacing: 10,
		},
		Children: []decl.Widget{
			decl.PushButton{
				Text:      "インストール",
				OnClicked: onInstallButton,
			},
			decl.PushButton{
				Text:      "終了",
				OnClicked: onQuitButton,
			},
		},
	}
}

func onQuitButton() {
	mainWin.Close(0)
}

func onInstallButton() {
	saveConfig()

	engine := binding.engines[binding.EngineID]
	editor := binding.editors[binding.EditorID]

	if engine.ID|editor.ID == 0 {
		inform("インストールするエンジンとエディタを選択してください。", mainWin)
		return
	}
	if !confirm("インストールを開始します。よろしいですか？"+
		"\nエンジン: "+engine.Name+
		"\nエディタ: "+editor.Name,
		mainWin) {
		return
	}

	var win *walk.Dialog
	var msg *walk.Label
	var pb *walk.ProgressBar

	size := decl.Size{Width: 360, Height: 100}

	decl.Dialog{
		AssignTo:  &win,
		Title:     "しばらくお待ちください",
		Icon:      rsrcIco,
		MinSize:   size,
		MaxSize:   size,
		FixedSize: true,
		Background: decl.SolidColorBrush{
			Color: walk.RGB(0xff, 0xff, 0xff),
		},
		Font: decl.Font{
			Family:    font,
			PointSize: 9,
		},
		Layout: decl.VBox{},
		Children: []decl.Widget{
			decl.Label{
				AssignTo: &msg,
			},
			decl.ProgressBar{
				AssignTo: &pb,
				MaxValue: 1000,
			},
		},
	}.Create(mainWin)

	canceling := win.Closing().Attach(cancelClosing)

	ch := make(chan progress)
	go startInstall(ch, win)

	go func() {
		defer func() {
			win.Closing().Detach(canceling)
			win.Close(0)
		}()

		for p := range ch {
			switch p.Stat {
			case onDownload, onComplete:
				if pb.MarqueeMode() {
					pb.SetMarqueeMode(false)
				}
			default:
				if !pb.MarqueeMode() {
					pb.SetMarqueeMode(true)
				}
			}
			if len(p.Message) > 0 {
				msg.SetText(p.Message)
			}
			pb.SetValue(p.Permill)
		}
	}()

	win.Run()
}

func startInstall(ch chan progress, win walk.Form) {
	defer close(ch)
	if done, err := install(ch); err != nil {
		alert(err.Error(), win)
	} else if done {
		inform("最新版のインストールが完了しました。", win)
	} else {
		inform("最新版がインストールされています。", win)
	}
}

func cancelClosing(canceled *bool, reason walk.CloseReason) {
	*canceled = true
}
