cwpy-installer
==============

Author
: Iraka.T

Home
: https://irakat.bitbucket.io

Repository
: https://bitbucket.org/irakat/cwpy-installer

About
-----

このソフトウェアはCardWirthPyとcwxeditorの非公式なインストーラです。CardWirthPy Rebootとcwxeditorのダウンロードページから、自動的に最新版を探してインストールします。

cwpy-installerは無料で利用していただけますが無保証です。たとえばダウンロードページの仕様が変更されて、突然使えなくなるかもしれません。また、このソフトウェアの利用によっていかなる損害が生じても、製作者やCardWirthPy開発関係者は一切の責任を負いません。

Licenses
--------

LICENCE.mdを参照してください。