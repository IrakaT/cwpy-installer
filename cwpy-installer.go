package main

import (
	"errors"
	_ "image/png"
	"log"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/BurntSushi/toml"
	"github.com/lxn/walk"
	decl "github.com/lxn/walk/declarative"
)

const (
	appName  = "cwpy-installer"
	appVer   = "0.2"
	appTitle = appName + " ver." + appVer
	rsrcIco  = 8
	font     = "Meiryo UI"
)

const (
	verNone   = iota
	verStable = iota
	verBeta   = iota
	verAlpha  = iota
	verDaily  = iota
)

const (
	onReady    = iota
	onCheck    = iota
	onDownload = iota
	onInstall  = iota
	onComplete = iota
	onError    = iota
)

var (
	exeFile string
	cfgFile string
	exeDir  string
	dlDir   string

	data tomlData

	rootWin      *walk.MainWindow
	mainWin      *walk.Dialog
	submitButton *walk.PushButton
	cancelButton *walk.PushButton
	binder       *walk.DataBinder
	binding      bindData

	reVer = regexp.MustCompile(
		`_([0-9]+)\.([0-9]+)(?:(alpha|beta)([0-9]+))?([a-z]?)`)
)

func init() {
	log.Printf("アプリケーションを開始: %s", appTitle)

	// 親窓をフルスクリーン化しておくと子窓が画面中央に来る
	// ただし Visible: false ではタスクバーにも表示されない
	decl.MainWindow{
		AssignTo: &rootWin,
		Title:    appTitle,
		Visible:  false,
	}.Create()
	rootWin.SetFullscreen(true)

	if path, err := os.Executable(); err != nil {
		alert("実行ファイルのパス取得に失敗しました。", rootWin)
		log.Panic(err)
	} else if path, err := filepath.EvalSymlinks(path); err != nil {
		alert("実行ファイルのパス解決に失敗しました。", rootWin)
		log.Panic(err)
	} else {
		log.Printf("実行ファイル: %s", path)
		x := len(path) - len(filepath.Ext(path))
		exeFile = path
		cfgFile = path[0:x] + ".toml"
		exeDir = filepath.Dir(path)
		dlDir = path[0:x]
	}

	if err := loadToml(); err != nil {
		alert(err.Error(), rootWin)
		log.Panic(err)
	} else {
		loadConfig()
	}
}

func quit() {
	// saveToml() は install.go install() 内
	// 終了時では多重起動時に意図せず上書きしてしまう

	// saveConfig() は widgets.go onInstallButton() 内

	// ここでダウンロードファイルの削除をしていたが
	// 簡便にダウングレードを行えるよう削除しないことに

	log.Println("アプリケーションを終了")
}

func main() {
	defer quit()

	size := decl.Size{Width: 400}

	// MainWindow は最大化・最小化ボタンが強制表示
	// ウィンドウサイズを固定したいので Dialog を使う
	decl.Dialog{
		AssignTo:  &mainWin,
		Title:     appTitle,
		Icon:      rsrcIco,
		MinSize:   size,
		MaxSize:   size,
		FixedSize: true,
		Background: decl.SolidColorBrush{
			Color: walk.RGB(0xff, 0xff, 0xff),
		},
		DataBinder: decl.DataBinder{
			AssignTo:   &binder,
			DataSource: &binding,
		},
		Font: decl.Font{
			Family:    font,
			PointSize: 10,
		},
		Layout: decl.VBox{
			MarginsZero: true,
			SpacingZero: true,
		},
		Children: []decl.Widget{
			headerWidget(),
			optionsWidget(),
			manualWidget(),
			buttonsWidget(),
		},
	}.Create(nil)
	// タスクバーに表示させたいので、親に rootWin を指定しない

	rootRect := rootWin.Bounds()
	mainRect := mainWin.Bounds()
	mainRect.X = (rootRect.Width - mainRect.Width) / 2
	mainRect.Y = (rootRect.Height - mainRect.Height) / 2
	mainWin.SetBounds(mainRect)

	mainWin.Run()
}

func loadDefaultToml() (data tomlData) {
	toml.Decode(string(MustAsset("default.toml")), &data)
	return
}

func loadToml() error {
	if _, err := os.Stat(cfgFile); err != nil {
		log.Println("デフォルト設定を使用")
		data = loadDefaultToml()
	} else {
		log.Printf("設定ファイルを読込: %s", cfgFile)
		if _, err := toml.DecodeFile(cfgFile, &data); err != nil {
			return errors.New("設定ファイルの読込に失敗しました。")
		}
		if data.Engine.PlaceID < 0 || data.Engine.PlaceID >= len(data.Engine.Places) ||
			data.Editor.PlaceID < 0 || data.Editor.PlaceID >= len(data.Editor.Places) {
			return errors.New("設定データに問題: PlaceIDの値が不正です。")
		}
		if data.Engine.Needs < 0 || data.Engine.Needs > verDaily ||
			data.Editor.Needs < 0 || data.Editor.Needs > verDaily {
			return errors.New("設定データに問題: Needsの値が不正です。")
		}
		for _, place := range append(data.Engine.Places, data.Editor.Places...) {
			if _, err := url.Parse(place.URL); err != nil {
				return errors.New("設定データに問題: URLの値が不正です。")
			}
			if _, err := regexp.Compile(strings.Join(place.Archives[:], "|")); err != nil {
				return errors.New("設定データに問題: Archivesの値が不正です。")
			}
		}
	}
	if data.AppVer != appVer {
		log.Printf("設定データを更新: AppVer %s -> %s", data.AppVer, appVer)
		data.update(loadDefaultToml())
	}
	return nil
}
func saveToml() error {
	log.Printf("設定ファイルを書出: %s", cfgFile)
	buf, err := os.OpenFile(cfgFile, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0666)
	if err != nil {
		return errors.New("設定ファイルの書出に失敗しました。")
	}
	defer buf.Close()
	toml.NewEncoder(buf).Encode(data)
	return nil
}

func loadConfig() {
	log.Println("設定データを入力初期値へ")
	archsLen := 4
	binding.EngineID = archsLen*data.Engine.PlaceID + data.Engine.Needs
	binding.EditorID = archsLen*data.Editor.PlaceID + data.Editor.Needs
	binding.SubFolder = data.Editor.SubFolder
	binding.engines = data.Engine.model()
	binding.editors = data.Editor.model()
}
func saveConfig() {
	log.Println("入力現在値を設定データへ")
	binder.Submit()
	engine := binding.engines[binding.EngineID]
	data.Engine.PlaceID = engine.PlaceID
	data.Engine.Needs = engine.Needs
	editor := binding.editors[binding.EditorID]
	data.Editor.PlaceID = editor.PlaceID
	data.Editor.Needs = editor.Needs
	data.Editor.SubFolder = binding.SubFolder
}

func confirm(msg string, owner walk.Form) bool {
	ans := walk.MsgBox(owner, "", msg, walk.MsgBoxYesNo|walk.MsgBoxIconQuestion)
	return ans == 6
}
func inform(msg string, owner walk.Form) {
	walk.MsgBox(owner, "", msg, walk.MsgBoxOK|walk.MsgBoxIconInformation)
}
func alert(msg string, owner walk.Form) {
	walk.MsgBox(owner, "", msg, walk.MsgBoxOK|walk.MsgBoxIconExclamation)
}
