package main

import (
	"archive/zip"
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"unsafe"

	"golang.org/x/net/html/charset"
	"golang.org/x/text/transform"
)

func install(ch chan progress) (done bool, err error) {
	defer func() {
		if done {
			saveToml()
		}
	}()

	for _, app := range []*appData{&data.Engine, &data.Editor} {
		if app.Needs > 0 {
			ret, err := _install(app, ch)
			done = done || ret
			if err != nil {
				return done, err
			}
		}
	}

	ch <- progress{onComplete, 1000, "インストール完了"}
	return done, nil
}

func _install(app *appData, ch chan progress) (done bool, err error) {
	place := &app.Places[app.PlaceID]
	list, err := _listup(app, place, ch)
	if err != nil {
		return done, err
	}

	archs := [][]string{place.Archives[:app.Needs], {}}
	if app.Needs >= verDaily {
		archs[1] = archs[0][verDaily-1:]
		archs[0] = archs[0][:verDaily-1]
	}

	ignore := app.Ignore
	if app.Needs < verDaily {
		if len(ignore[1]) > 0 {
			ignore[0] = ""
		}
		ignore[1] = ""
	}

	var names [2]string
	var releases []string

	re := regexp.MustCompile("^(?:" + strings.Join(archs[0], "|") + ")$")
	for _, item := range list {
		if re.MatchString(item) {
			releases = append(releases, item)
		}
	}
	if len(releases) == 0 {
		return done, errors.New(place.Name + "にリリース版のアーカイブを見つけられません。")
	}

	names[0] = _sort(releases)[0]

	if app.Needs >= verDaily {
		re := regexp.MustCompile("^(?:" + strings.Join(archs[1], "|") + ")$")
		if re.MatchString(list[0]) {
			names[1] = list[0]
		}
	}

	for i, name := range names {
		if ignore[i] != name && len(name) > 0 {
			ch <- progress{onDownload, 0, "ダウンロード中: " + name}
			zipFile, err := _download(place, name, ch)
			if (err) != nil {
				ch <- progress{onError, 0, "ダウンロード失敗: " + name}
				return done, err
			}

			ch <- progress{onInstall, 0, "インストール中: " + name}
			if err := _expand(zipFile, place.Folder, app.SubFolder); err != nil {
				ch <- progress{onError, 0, "インストール失敗: " + name}
				return done, err
			}

			ch <- progress{onReady, 0, "インストール完了: " + name}
			ignore[i] = name
			done = true
		}
	}

	app.Ignore = ignore
	return done, nil
}

func _listup(app *appData, place *placeData, ch chan progress) ([]string, error) {
	ch <- progress{onCheck, 0, place.Name + "の最新版を確認中"}

	urlobj, err := url.Parse(place.URL)
	if err != nil {
		return nil, errors.New(place.Name + "のURLが不正です。")
	}

	resp, err := http.Get(place.URL)
	if err != nil {
		return nil, errors.New(place.Name + "への接続に失敗しました。")
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return nil, errors.New(place.Name + "からの応答が正しくありません。(" + resp.Status + ")")
	}

	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.New(place.Name + "の取得中に問題が発生しました。")
	}

	host := regexp.QuoteMeta(urlobj.Host)
	path := regexp.QuoteMeta(urlobj.Path)
	name := strings.Join(place.Archives[:app.Needs], "|")
	uri := fmt.Sprintf(`(?:(?:%s:)?(?://%s)?%s|\./)(%s)`,
		urlobj.Scheme, host, path, name)
	pattern := regexp.MustCompile(`<a\b[^<>]*\shref="` + uri + `"[^<>]*>`)
	matches := pattern.FindAllSubmatch(buf, -1)

	size := len(matches)

	if size == 0 {
		return nil, errors.New(place.Name + "に該当するファイルを発見できません。")
	}

	list := make([]string, size)
	for i, item := range matches {
		list[i] = string(item[1])
	}
	return list, nil
}

func _sort(list []string) []string {
start:
	for i, j, x := 0, 1, len(list); j < x; i, j = i+1, j+1 {
		if reVer.MatchString(list[i]) && reVer.MatchString(list[j]) {
			a := reVer.FindStringSubmatch(list[i])
			b := reVer.FindStringSubmatch(list[j])
			for k, x := 1, len(a); k < x; k++ {
				a, b := a[k], b[k]
				if a != b {
					switch k {
					case 3: // alpha or beta or ""
						if len(b) == 0 || (b == "beta" && a == "alpha") {
							goto swap
						}
					case 5: // [a-z]?
						if len(a) < len(b) || a < b {
							goto swap
						}
					default: // [0-9]+
						a, _ := strconv.Atoi(a)
						b, _ := strconv.Atoi(b)
						if a < b {
							goto swap
						}
					}
					break
				swap:
					list[i], list[j] = list[j], list[i]
					goto start
				}
			}
		}
	}
	return list
}

func _download(place *placeData, name string, ch chan progress) (string, error) {
	remoteName := place.URL + name
	localName := filepath.Join(dlDir, name)

	if err := _putDir(dlDir, 0777); err != nil {
		return localName, errors.New("ダウンロードフォルダの作成に失敗しました。")
	}

	if stat, err := os.Stat(localName); err == nil && !stat.IsDir() {
		return localName, nil
	}

	file, err := os.Create(localName)
	if err != nil {
		return localName, errors.New(name + "のファイル作成に失敗しました。")
	}
	defer func() {
		file.Close()
		if stat, err := os.Stat(localName); err != nil && stat.Size() == 0 {
			os.Remove(localName)
		}
	}()

	resp, err := http.Get(remoteName)
	if err != nil {
		return localName, errors.New(name + "への接続に失敗しました。")
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return localName, errors.New(name + "からの応答が正しくありません。(" + resp.Status + ")")
	}

	if ch != nil {
		ch <- progress{onDownload, 0, ""}
	}

	length := resp.ContentLength
	writer := dlWriter{Writer: file, length: length, ch: ch}
	if _, err := io.Copy(&writer, resp.Body); err != nil {
		file.Seek(0, 0)
		file.Truncate(0)
		return localName, errors.New(name + "の取得中に問題が発生しました。")
	}

	return localName, nil
}

func _expand(zipFile, prefix string, subFolder bool) error {
	unzipper, err := zip.OpenReader(zipFile)
	if err != nil {
		return errors.New(filepath.Base(zipFile) + "を開くことができません。インストールを中止します。")
	}
	defer unzipper.Close()

	outDir := exeDir
	if subFolder {
		outDir = filepath.Join(exeDir, prefix)
		if err := _putDir(outDir, 0777); err != nil {
			return errors.New(prefix + err.Error() + "しました。インストールを中止します。")
		}
	}

	var done bool

	re := regexp.MustCompile("^" + regexp.QuoteMeta(prefix) + "/(.+)$")
	for _, file := range unzipper.File {
		var fullname string

		filename := _nosjis(file.Name)

		if m := re.FindStringSubmatch(filename); m != nil {
			fullname = filepath.Join(outDir, m[1])
		} else {
			continue
		}

		var err error
		if file.FileInfo().IsDir() {
			err = _putDir(fullname, file.Mode())
		} else {
			err = _putFile(file, fullname)
		}
		if err != nil {
			return errors.New(filename + err.Error() + "しました。不完全なインストール状態です。")
		}

		done = true
	}

	if !done {
		return errors.New(filepath.Base(zipFile) + "は" + prefix + "のアーカイブではないようです。")
	}

	return nil
}

func _nosjis(str string) string {
	var buf bytes.Buffer
	e, _ := charset.Lookup("sjis")
	w := transform.NewWriter(&buf, e.NewDecoder())
	defer w.Close()
	if _, err := w.Write(*(*[]byte)(unsafe.Pointer(&str))); err == nil {
		return string(buf.Bytes())
	}
	return str
}

func _putDir(name string, mode os.FileMode) error {
	if _, err := os.Stat(name); err != nil {
		if err := os.MkdirAll(name, mode); err != nil {
			return errors.New("フォルダの作成に失敗")
		}
	}
	return nil
}

func _putFile(file *zip.File, outName string) error {
	in, err := file.Open()
	if err != nil {
		return errors.New("ファイルの展開に失敗")
	}
	defer in.Close()

	out, err := os.OpenFile(outName, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, file.Mode())
	if err != nil {
		return errors.New("ファイルの作成に失敗")
	}
	defer func() {
		out.Close()
		time := file.ModTime()
		os.Chtimes(outName, time, time)
	}()

	if _, err := io.Copy(out, in); err != nil {
		return errors.New("ファイルの書込に失敗")
	}

	return nil
}
