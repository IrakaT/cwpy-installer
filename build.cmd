@echo off
setlocal
cd /d "%~dp0"
set project=cwpy-installer
set guiflag=-H=windowsgui
if "%~1" equ "debug" set guiflag=
set GOOS=windows
set GOARCH=386
if not exist "%project%.go" goto:eof
go-bindata -nometadata -prefix resource resource
if not errorlevel 0 goto:eof
go build -o "bin/%project%.exe" -ldflags "-w -s %guiflag%"
