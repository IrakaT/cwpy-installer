package main

import (
	"io"
)

type tomlData struct {
	AppVer string
	Engine appData
	Editor appData
}

func (o *tomlData) update(newData tomlData) {
	o.AppVer = newData.AppVer
ENGINE:
	for _, place := range newData.Engine.Places {
		for i, currentPlace := range o.Engine.Places {
			if place.Name == currentPlace.Name {
				o.Engine.Places[i] = place
				continue ENGINE
			}
		}
		o.Engine.Places = append(o.Engine.Places, place)
	}
EDITOR:
	for _, place := range newData.Editor.Places {
		for i, currentPlace := range o.Editor.Places {
			if place.Name == currentPlace.Name {
				o.Editor.Places[i] = place
				continue EDITOR
			}
		}
		o.Editor.Places = append(o.Editor.Places, place)
	}
}

type appData struct {
	PlaceID   int
	Needs     int
	SubFolder bool
	Ignore    [2]string
	Places    []placeData
}
type placeData struct {
	Name     string
	URL      string
	Folder   string
	Archives [4]string
}
type archiveData struct {
	ID      int
	PlaceID int
	Needs   int
	Name    string
	URL     string
	Folder  string
	Pattern string
}
type bindData struct {
	EngineID  int
	EditorID  int
	SubFolder bool

	engines []*archiveData
	editors []*archiveData
}

func (o appData) model() []*archiveData {
	labels := []string{
		"正式リリース版",
		"βテスト版まで",
		"αテスト版まで",
		"デイリービルド",
	}
	list := []*archiveData{{Name: "なし"}}
	for placeID, place := range o.Places {
		for i, pattern := range place.Archives {
			list = append(list, &archiveData{
				ID:      len(list),
				PlaceID: placeID,
				Needs:   i + 1,
				Name:    place.Name + " " + labels[i],
				URL:     place.URL,
				Folder:  place.Folder,
				Pattern: pattern,
			})
		}
	}
	return list
}

type progress struct {
	Stat    int
	Permill int
	Message string
}

type dlWriter struct {
	io.Writer
	length int64
	total  int64
	ch     chan progress
}

func (o *dlWriter) Write(p []byte) (int, error) {
	n, err := o.Writer.Write(p)
	o.total += int64(n)
	if err == nil && o.length > 0 && o.ch != nil {
		permill := int(1000 * o.total / o.length)
		o.ch <- progress{onDownload, permill, ""}
	}
	return n, err
}
